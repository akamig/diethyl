import React from 'react';
import styles from './ArticleBody.module.scss';

const ArticleBody = ({ html }) => (
  <div className={styles['body']}>
    <div
      className={styles['body__html']}
      dangerouslySetInnerHTML={{ __html: html }}
    />
  </div>
);

export default ArticleBody;
