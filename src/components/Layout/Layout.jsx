import React from 'react';
import Helmet from 'react-helmet';
import Sidebar from '../Sidebar/Sidebar';
import styles from './Layout.module.scss';

const Layout = ({ children }) => {
  return (
    <div className={styles.layout}>
      <Helmet>
        <html lang="ko" />
      </Helmet>
      <Sidebar className={styles['sidebar']} />
      <div className={styles['children']}>{children}</div>
    </div>
  );
};

export default Layout;
