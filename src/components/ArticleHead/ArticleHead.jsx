import React from 'react';
import styles from './ArticleHead.module.scss';
import { Link } from 'gatsby';

const ArticleInfo = ({ post }) => {
  return (
    <div className={styles['header']}>
      <h1 className={styles['header__title']}>
        <Link to={post.fields.slug}>{post.frontmatter.title}</Link>
      </h1>
      <div className={styles['header__meta']}>
        <span>{post.frontmatter.date}</span>
        <Link><span>{post.frontmatter.category}</span></Link>
        <span>{post.timeToRead} Minute</span>
      </div>
      <hr />
    </div>
  );
};
export default ArticleInfo;
