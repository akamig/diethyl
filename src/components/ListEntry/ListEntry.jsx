import React from 'react';
import { Link } from 'gatsby';
import styles from './ListEntry.module.scss';

const ListEntry = ({ node }) => {
  return (
    <Link to={node.fields.slug}>
      <div className={styles['listentry']}>
        <h2 className={styles['listentry__title']}>{node.frontmatter.title}</h2>
        <span className={styles['listentry__date']}>
          {node.frontmatter.date}
        </span>
        <p className={styles['listentry__desc']}>{node.excerpt}</p>
      </div>
    </Link>
  );
};

export default ListEntry;
