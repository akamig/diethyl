import React from 'react';
import { Link } from 'gatsby';
import Logo from '../Logo/Logo';
import Toggle from '../../assets/images/category.inline.svg';
import styles from './Sidebar.module.scss';

export const Sidebar = () => {
  return (
    <aside className={styles['sidebar']}>
      <nav className={styles['nav']}>
        <Logo />
        <a className={styles['toggle']}>
          <Toggle />
        </a>
        <ul>
          <li>
            <a href="/category">Category</a>
          </li>
          <li>
            <a href="/tags">Tags</a>
          </li>
          <li>
            <a href="/about">About</a>
          </li>
          <li>
            <a href="/badges">Badges</a>
          </li>
        </ul>
      </nav>
    </aside>
  );
};

export default Sidebar;
