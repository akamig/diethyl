import React from 'react';
import { Link } from 'gatsby';
import Icon from '../../assets/images/akamig.svg';

export const Logo = () => {
  return (
    <Link to="/">
      <Icon />
    </Link>
  );
};

export default Logo;