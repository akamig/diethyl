import React from 'react';
import ArticleBody from '../ArticleBody/ArticleBody';
import ArticleHead from '../ArticleHead/ArticleHead';
import styles from './Article.module.scss';

const Article = ({ data }) => {
  const post = data.markdownRemark;
  return (
    <article className={styles['article']}>
      <ArticleHead post={post} />
      <ArticleBody html={post.html} />
    </article>
  );
};

export default Article;
