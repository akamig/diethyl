import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../components/Layout/Layout';
import Article from '../components/Article/Article';

const ArticleTemplate = ({ data }) => (
  <Layout>
    <Article data={data} />
  </Layout>
);

export default ArticleTemplate;

export const articleQuery = graphql`
  query Postbyslug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      html
      timeToRead
      frontmatter {
        title
        date(formatString: "YYYY. MM. DD. dddd, hh:mm:ss A")
        category
        tags
      }
      fields {
        slug
      }
      tableOfContents
    }
  }
`;
