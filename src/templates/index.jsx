import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../components/Layout/Layout';
import ListEntry from '../components/ListEntry/ListEntry';

const Index = ({ data }) => {
  return (
    <Layout>
      {data.allMarkdownRemark.edges.map(({ node }) => (
        <ListEntry node={node} />
      ))}
    </Layout>
  );
};

export const pageQuery = graphql`
  query {
    allMarkdownRemark {
      totalCount
      edges {
        node {
          id
          excerpt(pruneLength: 140, truncate: true)
          timeToRead
          frontmatter {
            category
            date(formatString: "YYYY. MM. DD. dddd, hh:mm:ss A")
            tags
            title
            description
          }
          fields {
            slug
          }
        }
      }
    }
  }
`;
export default Index;
