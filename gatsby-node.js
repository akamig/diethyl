const path = require('path');
const { createFilePath } = require('gatsby-source-filesystem');

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions;

  if (node.internal.type === 'MarkdownRemark') {
    if (typeof node.frontmatter.slug !== 'undefined') {
      createNodeField({
        node,
        name: 'slug',
        value: node.frontmatter.slug,
      });
    } else {
      const slug = createFilePath({ node, getNode });
      createNodeField({
        node,
        name: 'slug',
        value: slug,
      });
    }
  }
};
exports.createPages = async ({ graphql, actions }) => {
  const Article = path.resolve('./src/templates/ArticleTemplate.jsx');
  const { createPage } = actions;

  const { data } = await graphql(`
    query AllMarkdown {
      allMarkdownRemark(
        sort: { order: DESC, fields: frontmatter___date }
        limit: 1000
      ) {
        edges {
          node {
            frontmatter {
              title
              date
              category
              tags
              description
            }
            html
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  data.allMarkdownRemark.edges.forEach(({ node }) => {
    actions.createPage({
      path: node.fields.slug,
      component: Article,
      context: { slug: node.fields.slug },
    });
  });

  createPage({
    path: '/404',
    component: path.resolve('./src/templates/404.jsx'),
  });

  createPage({
    path: '/',
    component: path.resolve('./src/templates/index.jsx'),
  });
};
